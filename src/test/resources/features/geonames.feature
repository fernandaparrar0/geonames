Feature: As a user of the Geonames api
  I want to get some specific data of the country obtained with the coordinates
  to know information about it

  Scenario Outline: get some specific data of the country
    Given user gives necessary data
      |formatted | <formatted>|
      | lat      | <latitud>  |
      | lng      | <longitud> |
      | username | <username> |
      | style    | <style>    |
    When user fetch data Geoname api
    Then user verifies data
    Examples:
      |formatted | latitud | longitud | username       | style |
      |true      |-10      | 20       | qa_mobile_easy | full  |


  Scenario Outline: user sends invalid parameters
    Given user gives necessary invalid data
      |formatted | <formatted>|
      | lat      | <latitud>  |
      | Ing      | <longitud> |
      | username | <username> |
      | style    | <style>    |
    When user fetch invalid data Geoname api
    Then user verifies invalid parameters response
    Examples:
      |formatted | latitud | longitud | username       | style |
      |true      |-10      | 20       | qa_mobile_easy | full  |