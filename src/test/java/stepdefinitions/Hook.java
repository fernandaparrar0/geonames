package stepdefinitions;

import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import java.util.ResourceBundle;

public class Hook {

    private final static String ENDPOINT_GEONAMES = ResourceBundle.getBundle("serenityEndpoint").getString("endPointGeonames");

    @Before()
    public void configuration() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("fernanda");
        OnStage.theActorInTheSpotlight().whoCan(CallAnApi.at(ENDPOINT_GEONAMES));
    }
}
