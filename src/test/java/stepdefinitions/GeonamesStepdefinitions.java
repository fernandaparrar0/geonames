package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import questions.InvalidParams;
import questions.Response;
import tasks.FetchGeonamesApi;
import tasks.LoadData;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import java.util.Map;

public class GeonamesStepdefinitions {

    @Given("^user gives necessary data$")
    public void userGivesNecessaryData(Map<String, Object> params) {
        theActorInTheSpotlight().wasAbleTo(LoadData.loadData(params));
    }

    @When("^user fetch data Geoname api$")
    public void userFetchDataGeonameApi() {
        theActorInTheSpotlight().attemptsTo(FetchGeonamesApi.fetchGeonamesApi());
        
    }

    @Then("^user verifies data$")
    public void userVerifiesData() {
        theActorInTheSpotlight().should(seeThat(Response.response()));

    }

    @Given("^user gives necessary invalid data$")
    public void userGivesNecessaryInvalidData(Map<String, Object> params) {
        theActorInTheSpotlight().wasAbleTo(LoadData.loadData(params));
    }


    @When("^user fetch invalid data Geoname api$")
    public void userFetchInvalidDataGeonameApi() {
        theActorInTheSpotlight().attemptsTo(FetchGeonamesApi.fetchGeonamesApi());
    }

    @Then("^user verifies invalid parameters response$")
    public void userVerifiesInvalidParametersResponse() {
        theActorInTheSpotlight().should(seeThat(InvalidParams.invalidParams()));
    }
}
