package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;
import static org.hamcrest.CoreMatchers.equalTo;

public class InvalidParams {

    public static Question<Boolean> invalidParams() {
        return actor -> {
            actor.should(
                    ResponseConsequence.seeThatResponse(response ->
                            response.body("status.message", equalTo("missing parameter "))
                    )
            );

            return true;
        };
    }
}
