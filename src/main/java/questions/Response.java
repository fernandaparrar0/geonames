package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;
import static org.hamcrest.CoreMatchers.equalTo;

public class Response {

    public static Question<Boolean> response() {
        return actor -> {
            actor.should(
                    ResponseConsequence.seeThatResponse(response ->
                            response.body("lng", equalTo(20))
                                    .body("timezoneId", equalTo("Africa/Luanda"))
                                    .body("countryName",  equalTo("Angola"))
                                    .body("lat", equalTo(-10))
                    )
            );

            return true;
        };
    }
}
