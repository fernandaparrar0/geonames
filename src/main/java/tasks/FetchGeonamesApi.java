package tasks;

import models.GeonameData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class FetchGeonamesApi implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("").with(requestSpecification ->
                        requestSpecification.params(
                                GeonameData.getParams()))
        );
    }

    public static FetchGeonamesApi fetchGeonamesApi(){
        return Tasks.instrumented(FetchGeonamesApi.class);
    }
}
