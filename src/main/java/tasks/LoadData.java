package tasks;

import models.GeonameData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.Map;

public class LoadData implements Task {

    private final Map<String, Object> data;

    public LoadData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        GeonameData.setParams(data);
    }

    public static LoadData loadData(Map<String, Object> data){
        return Tasks.instrumented(LoadData.class, data);
    }
}
