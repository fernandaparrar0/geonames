package models;

import java.util.HashMap;
import java.util.Map;

public class GeonameData {

    private static Map<String, Object> params = new HashMap<>();

    public static Map<String, Object> getParams() {
        return params;
    }

    public static void setParams(Map<String, Object> params) {
        GeonameData.params = params;
    }
}
